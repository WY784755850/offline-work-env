# offlineWorkEnv

#### 介绍
portainer docker-compose 安装


#### 软件架构
```text
x86
```

#### 安装教程

```shell
# 导入镜像 有网的话直接忽略也好
docker load -i portainer-latest.tar 
#执行命令 
docker-compose up -d

```

#### 使用说明

```http request

# 访问 
http://127.0.0.1:9000/
# 页面
../picture/i![img.png](..%2Fpicture%2Fimg.png)

```
 